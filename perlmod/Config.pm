#!/usr/bin/perl

use strict;
use warnings;

package NuCraftd::Config;
our $VERSION = '1.0';

use Exporter;
use Getopt::Long qw(:config no_ignore_case bundling_override auto_help);
use Pod::Usage;
use NuCraftd::Base;
use NuCraftd::Util;

prodver('$Format:%h$');

our @ISA = ('NuCraftd::Base');
our @EXPORT = qw(
	configload
	debugmode
	defaultworld
	world
	rootpath
	temppath
	lockpath
	sockpath
	datapath
	statusprefix
	logprefix
	);

my $globalconfig;

sub config
	{
	$globalconfig or $globalconfig = bless({ }, 'NuCraftd::Config');
	}

sub configload
	{
	my $allowroot;

	my %conf = @_ or ( );
	$conf{'r=s'} = sub { shift; rootpath(@_) };
	$conf{'t=s'} = sub { shift; temppath(@_) };
	$conf{'w=s'} = sub { shift; world(@_) };
	$conf{'D'} = sub { debugmode(1) };
	$conf{'R'} = \$allowroot;
	$conf{'V'} = sub { print prodline() . "\n"; exit 0; };
	GetOptions(%conf) or pod2usage(2);

	$> or $allowroot or die('cannot run as root');

	appname();
	umask(022);

	checkdir(temppath());
	checkdir(rootpath());
	checkdir(datapath());
	}

sub debugmode
	{
	config()->getsetprop('debugmode', @_);
	}

sub defaultworld
	{
	'default';
	}

sub world
	{
	map { tr#A-Za-z0-9##cd; $_ = lc($_); } @_;
	config()->getsetprop('world', @_)
		or defaultworld();
	}

sub rootpath
	{
	config()->getsetprop('rootpath', @_)
		or $ENV{'HOME'} . '/.' . prodname();
	}

sub temppath
	{
	config()->getsetprop('temppath', @_)
		or '/tmp/' . prodname() . '-' . getpwuid($>);
	}

sub lockpath
	{
	config()->getsetprop('lockpath', @_)
		or temppath() . '/.' . world() . '-' . appname() . '.lock';
	}

sub sockpath
	{
	config()->getsetprop('sockpath', @_)
		or temppath() . '/' . world() . '.sock';
	}

sub datapath
	{
	config()->getsetprop('datapath', @_)
		or rootpath . '/' . world();
	}

sub statusprefix
	{
	config()->getsetprop('statusprefix', @_)
		or '<' . world() . '>';
	}

sub logprefix
	{
	config()->getsetprop('logprefix', @_)
		or '[' . world() . '] ';
	}

1;
