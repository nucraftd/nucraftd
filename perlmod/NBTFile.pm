#!/usr/bin/perl

use strict;
use warnings;

package NuCraftd::NBTFile;
our $VERSION = '1.0';

use Exporter;
use Fcntl qw(:flock);
use IO::Uncompress::Gunzip;
use IO::Compress::Gzip;
use IO::Handle;
use FileHandle;
use NuCraftd::NBTag;
use NuCraftd::LockedFile;
use NuCraftd::Util;

prodver('$Format:%h$');

our @ISA = ('NuCraftd::LockedFile', 'NuCraftd::NBTag');

sub new
	{
	bless({ }, shift());
	}

sub gz
	{
	return shift->getsetprop('NBT_GZ', @_);
	}

sub read
	{
	my $self = shift();
	my $path = shift();
	my $lock = shift();
	my $nofail = shift();
	my $timeout = shift();

	$self->allvalues($path, $lock, $timeout);
	$self->readfile(sub
		{
		my $fh = shift;
		my $tmp = '';
		if(read($fh, $tmp, 2) == 2)
			{
			seek($fh, -2, 1);
			$tmp = unpack('s>', $tmp);
			$self->gz($tmp == 8075);
			}
		else
			{
			$nofail or die('empty NBT file');
			return;
			}

		if($self->gz)
			{
			$fh = IO::Uncompress::Gunzip->new($fh, AutoClose => 0) or die($!);
			}

		$self->readtag($fh);
		});
	}

sub create
	{
	my $self = shift;
	my $path = shift;
	my $lock = shift;
	my $timeout = shift();

	open(my $lfh, '>>', $path) or die($!);
	close($lfh);

	defined($lock) or $lock = LOCK_EX;
	$self->read($path, $lock, 1, $timeout);
	}

sub write
	{
	my $self = shift;
	
	$self->allvalues(@_);
	$self->writefile(sub
		{
		my $fh = shift();
		my $nfh;
		$self->gz and $nfh = IO::Compress::Gzip->new($fh)
			or $nfh = $fh or die($!);
		$self->writetag($nfh);
		});
	}
