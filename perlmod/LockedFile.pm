#!/usr/bin/perl

use strict;
use warnings;

package NuCraftd::LockedFile;
our $VERSION = '1.0';

use Exporter;
use Fcntl;
use Fcntl qw(:flock);
use IO::Handle;
use FileHandle;
use NuCraftd::Base;
use NuCraftd::Util;

prodver('$Format:%h$');

our @ISA = ('NuCraftd::Base');

sub new
	{
	my $self = bless({ }, shift());
	$self->allvalues(@_);
	return $self;
	}

sub allvalues
	{
	my $self = shift();

	my $path = shift();
	defined($path) or $path = $self->path();
	defined($path) and $self->path($path);

	my $lock = shift();
	defined($lock) or $lock = $self->lock();
	defined($lock) or $lock = LOCK_SH;
	$self->lock($lock);

	my $timeout = shift();
	defined($timeout) or $timeout = $self->timeout();
	defined($timeout) or $timeout = 5;
	$self->timeout($timeout);
	}

sub path
	{
	return shift()->getsetprop('LockFile_Path', @_);
	}

sub tmppath
	{
	my $path = shift()->path();
	$path =~ m#/# and $path =~ s#^(.*)/(.*?)$#$1/.new-$2# or $path = '.new-' . $path;
	return $path;
	}
	
sub fh
	{
	return shift()->getsetprop('LockFile_FH', @_);
	}

sub lock
	{
	return shift()->getsetprop('LockFile_LockLevel', @_);
	}

sub timeout
	{
	return shift()->getsetprop('LockFile_Timeout', @_);
	}

sub diewrapper
	{
	my $self = shift();
	my $r = eval { shift()->(); };
	if($@)
		{
		my $x = $@;
		chomp($x);
		die($x . '  path: ' . ($self->path() || 'NONE'));
		}
	return $r;
	}

sub timedflock
	{
	my $self = shift();
	my $fh = shift();
	my $lock = shift();
	my $timeout = shift();

	$self->diewrapper(sub
		{
		eval
			{
			local $SIG{'ALRM'} = sub { die('timeout locking file'); };
			alarm $timeout;
			flock($fh, $lock) or die($!);
			alarm 0;
			};
		$@ and die($@);
		});
	}

sub readfile
	{
	my $self = shift();
	my $readsub = shift();

	$self->diewrapper(sub
		{
		$self->allvalues(@_);

		my $fh;
		open($fh, '<', $self->path()) or die($!);
		$self->timedflock($fh, $self->lock(), $self->timeout());

		$self->fh($fh);

		return $readsub->($fh, $self);
		});
	}

sub createfile
	{
	my $self = shift();
	my $readsub = shift();

	$self->diewrapper(sub
		{
		$self->allvalues(@_);

		open(my $lfh, '>>', $self->path) or die($!);
		close($lfh);

		return $self->readfile($readsub);
		});
	}

sub writefile
	{
	my $self = shift();
	my $writesub = shift();

	$self->diewrapper(sub
		{
		$self->allvalues(@_);

		$self->fh() or $self->createfile(sub { });
		$self->timedflock($self->fh(), LOCK_EX, $self->timeout());

		open(my $nfh, '>', $self->tmppath()) or die($!);
		my $zult = $writesub->($nfh, $self);
		$nfh->flush();
		$nfh->sync();
		close($nfh) or die($!);

		rename($self->tmppath(), $self->path) or die($!);

		return $zult;
		});
	}

sub DESTROY
	{
	my $self = shift;
	$self->fh() and close($self->fh());
	}
