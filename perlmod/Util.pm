#!/usr/bin/perl

use strict;
use warnings;

package NuCraftd::Util;
our $VERSION = '1.0';

use Cwd;
use Exporter;
use File::Which;
our @ISA = 'Exporter';
our @EXPORT = qw(
	appname
	prodname
	prodver
	prodline
	checkdir
	flexsearch
	findexec
	sysordie
	constrip
	rxcmdop
	rxcmdtry
	rxcmdany
	rxchat
	rxemote
	rxjoin
	rxpart
	rxplist
	rxversion
	);

my $appnamesaved;
my $savedprodver;

sub appname
        {
        $appnamesaved and return $appnamesaved;
        my $a = $0;
        $a =~ s#^\s+##;
        $a =~ s#\s.*##;
        $a =~ s#^\S*/##;
        $appnamesaved = $a;
        }

sub prodname
        {
        'nucraftd';
        }

sub prodver
	{
	my $newver = shift();
	defined($newver) and ($newver =~ m#^\$F[o]rmat:#
		and $newver = 'development version' or $newver = 'version 0x' . $newver);
	defined($savedprodver) and defined($newver)
		and $newver ne $savedprodver
		and die('attempted to mix ' . prodname() . ' modules with different versions ('
			. $savedprodver . ' and ' . $newver . ')');
	!defined($savedprodver) and !defined($newver)
		and die('must set product version on first call to prodver()');
	!defined($savedprodver) and $savedprodver = $newver;
	return $savedprodver;
	}

sub prodline
	{
	prodname() . ' ' . prodver();
	}

sub checkdir
        {
        my $d = shift;
        defined($d) or die('checkdir called with null dir');
        -d $d and return 1;
        $d =~ m#(.+)/([^/]+)/*$# and checkdir($1);
        mkdir($d) or -d $d or die ('failed to create dir ' . $d); # recheck -d in case of race cond.
        return 1;
        }

sub strans
	{
	my $search = shift();
	chomp($search);
	$search =~ tr#'##d;
	$search =~ tr#A-Za-z0-9# #c;
	$search =~ s#\s+# #g;
	$search =~ s#^\s+##;
	$search =~ s#\s+$##;
	return $search;
	}

sub flexsearch
	{
	my $raw = shift();
	my $search = strans($raw);

	my %db = map { $_ => strans($_) } @_;

	my @exact1 = ();
	my @exact2 = ();
	foreach my $k ( keys %db )
		{
		lc($k) eq lc($raw) and push @exact1, $k;
		$db{$k} eq $search and push @exact2, $k;
		}
	scalar(@exact1) == 1 and return \@exact1;
	scalar(@exact2) == 1 and return \@exact2;

	my @results = ( );
	@_ = split(m/\s+/, $search);
	foreach my $k ( keys %db )
		{
		my $match = 1;
		my $n = $db{$k};
		foreach my $s ( @_ )
			{
			$n =~ m#\Q$s\E#i and $n =~ s#\S*\Q$s\E\S*##i
				or $match = '' or last;
			}
		$match or next;
		push @results, $k;
		}

	return \@results;
	}

sub findexec
	{
	my $e = shift();
	my $f = which($e);
	$f =~ m#\S# and -x $f or die('cannot find "' . $e . '" in path');
	return $f;
	}

sub sysordie
	{
	local $SIG{'CHLD'} = '';
	my $r = system(@_);
	$r == 0 and return 1;
	my $msg = 'shell cmd "' . join(' ', @_) . '" cwd "' . getcwd() . '" ';
    	$r == -1 and die($msg . 'failed to execute: ' . $!);
    	$r & 127 and die($msg . 'died with signal ' . ($r & 127) . (($r & 128) ? ', core dumped' : ''));
	die($msg . 'exit status ' . ($r >> 8));
	}

sub constrip
	{
	my $line = $_[0];
	# CR character is used to "reset" a line.  Strip off
	# anything before, and including, it (from Tekkit).
	$line =~ s#.*\r##s;
	# Strip out ANSI color codes.
	$line =~ s#\x1B\[.*?m##g;
	# Strip leading dates.
	$line =~ s#^\s*\d{4}-\d\d-\d\d\s*##;
	# Strip leading times.
	$line =~ s#^\s*\d\d:\d\d:\d\d\s*##;
	return $line;
	}

sub rxcmdop
	{
	my ($line, $sub) = @_;
	$line = constrip($line);
	$line =~ m#^\s*\[INFO\]\s+(\S+)\s+issued\s+server\s+command:\s+/?(.*)#
		and $sub->($1, $2);
	}

sub rxcmdtry
	{
	my ($line, $sub) = @_;
	$line = constrip($line);
	$line =~ m#^\s*\[INFO\]\s+(\S+)\s+tried\s+command:\s+/?(.*)#
		and $sub->($1, $2);
	}

sub rxcmdany
	{
	my ($line, $sub) = @_;
	$line = constrip($line);
	rxcmdop($line, $sub) or
	rxcmdtry($line, $sub);
	}

sub rxchat
	{
	my ($line, $sub) = @_;
	$line = constrip($line);
	$line =~ m#^\s*\[INFO\]\s+\<([^\>]+)\>\s+(.*)#
		and $sub->($1, $2);
	}

sub rxemote
	{
	my ($line, $sub) = @_;
	$line = constrip($line);
	$line =~ m#^\s*\[INFO\]\s+\*\s+(\S+)\s+(.*)#
		and $sub->($1, $2);
	rxcmdany($line, sub
		{
		my ($who, $what) = @_;
		$what =~ m#^\s*me\s+(.*)#i and $sub->($who, $1);
		});
	}

sub rxjoin
	{
	my ($line, $sub) = @_;
	$line = constrip($line);
	if($line =~ m#^\s*\[INFO\]\s+([^[\s]+)(\s*\[[^]]+\])?\s*logged\s+in\s#)
		{
		my $w = $1;
		$w =~ m#^\s*/# or $w =~ m#^\s*wp-#
			or $sub->($w);
		}
	}

sub rxpart
	{
	my ($line, $sub) = @_;
	$line = constrip($line);
	if($line =~ m#^\s*\[INFO\]\s+(\S+)\s+lost\s+connection#
		or $line =~ m#^\s*\[INFO\]\s+Disconnecting\s+(\S+)#
		or $line =~ m#^\s*\[INFO\]\s+\[\S+:\s+Kicked\s+(\S+)\s+from\s+the\s+game:\s#)
		{
		my $w = $1;
		$w =~ m#^\s*/# or $w =~ m#^\s*wp-#
			or $sub->($w);
		}
	}

sub rxversion
	{
	my ($line, $sub) = @_;
	$line = constrip($line);
	$line =~ m#^\s*\[INFO\]\s+Starting\s+minecraft\s+server\s+version\s+(.*)#
		and $sub->($1);
	}

sub rxplist
	{
	my ($line, $sub) = @_;
	$line = constrip($line);
	$line =~ m#^\s*\[INFO\]\s+\s*Connected\s+players:\s+(.*)#
		and $sub->(split(m#\s*,\s*#, $1));
	}

1;
