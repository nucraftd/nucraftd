#!/usr/bin/perl

use strict;
use warnings;

package NuCraftd::NBTag;
our $VERSION = '1.0';

use Exporter;
use IO::Handle;
use FileHandle;
use NuCraftd::Base;
use NuCraftd::Util;

prodver('$Format:%h$');

our @ISA = ('NuCraftd::Base');

our @EXPORT = qw(
	TAG_End
	TAG_Byte
	TAG_Short
	TAG_Int
	TAG_Long
	TAG_Float
	TAG_Double
	TAG_Byte_Array
	TAG_String
	TAG_List
	TAG_Compound
	);

use constant
	{
	TAG_End => 0,
	TAG_Byte => 1,
	TAG_Short => 2,
	TAG_Int => 3,
	TAG_Long => 4,
	TAG_Float => 5,
	TAG_Double => 6,
	TAG_Byte_Array => 7,
	TAG_String => 8,
	TAG_List => 9,
	TAG_Compound => 10
	};

sub new
	{
	bless({ }, shift());
	}

sub name
	{
	shift->getsetprop('NBT_Name', @_);
	}

sub type
	{
	shift->getsetprop('NBT_Type', @_);
	}

sub subtype
	{
	shift->getsetprop('NBT_SubType', @_);
	}

sub value
	{
	shift->getsetprop('NBT_Value', @_);
	}

sub subkeys
	{
	shift->getsetprop('NBT_SubKeys', @_);
	}

sub length
	{
	my $keys = shift->subkeys;
	defined($keys) and return scalar(@{$keys}) or return 0;
	}

sub readvalue
	{
	my $fh = shift;
	my $type = shift;

	my $sz = 0;
	my $spec = undef;

		{
		if($type == TAG_Byte) { $sz = 1; $spec = 'c'; last; }
		if($type == TAG_Short) { $sz = 2; $spec = 's>'; last; }
		if($type == TAG_Int) { $sz = 4; $spec = 'l>'; last; }
		if($type == TAG_Long)
			{
			# unpack format "q" not supported on all platforms.
			my $tmp = undef;
			8 == read($fh, $tmp, 8) or
				die('failed to read 8 bytes for type ' . $type);
			my ($la, $lb) = unpack('l>l>', $tmp);
			return $la * 4294967296 + $lb;
			}
		if($type == TAG_Float) { $sz = 4; $spec = 'f>'; last; }
		if($type == TAG_Double) { $sz = 8; $spec = 'd>'; last; }
		if($type == TAG_Byte_Array) { $sz = readvalue($fh, TAG_Int); last; }
		if($type == TAG_String) { $sz = readvalue($fh, TAG_Short); last; }
		die('unrecognized value type ' . $type);
		}

	my $tmp = undef;
	$sz == read($fh, $tmp, $sz) or die('failed to read ' . $sz . ' bytes for type ' . $type);
	defined($spec) and $tmp = unpack($spec, $tmp);
	return $tmp;
	}

sub readtag
	{
	my $self = shift();
	my $fh = shift();
	my $setname = shift();
	my $type = shift();

	defined($type) or $type = readvalue($fh, TAG_Byte);
	$self->type($type);

	!defined($setname) and $type != 0 and 
		$self->name(readvalue($fh, TAG_String));
	defined($setname) and $self->name($setname);

		{
		if($type == 0) { last; }
		if(($type >= 1) && ($type <= 8))
			{
			$self->value(readvalue($fh, $type));
			last;
			}
		if($type == 9)
			{
			my $subtype = readvalue($fh, TAG_Byte);
			my $length = readvalue($fh, TAG_Int);
			$self->subtype($subtype);
			my @skeys = ();
			for my $i ( 0 .. ($length - 1) )
				{
				my $stag = NuCraftd::NBTag->new();
				$stag->readtag($fh, $i, $subtype);
				$self->{$i} = $stag;
				push @skeys, $i;
				}
			$self->subkeys(\@skeys);
			last;
			}
		if($type == 10)
			{
			my @skeys = ();
			while(1)
				{
				my $stag = NuCraftd::NBTag->new();
				$stag->readtag($fh);
				$stag->type == 0 and last;
				$self->{$stag->name} = $stag;
				push @skeys, $stag->name;
				}
			$self->subkeys(\@skeys);
			last;
			}
		die('unexpected tag type ' . $type);
		}

	return $self;
	}

sub writevalue
	{
	my $fh = shift;
	my $type = shift;
	my $val = shift;

	my @topack = $val;
	my $spec = '';

		{
		if($type == TAG_Byte) { $spec = 'c'; last; }
		if($type == TAG_Short) { $spec = 's>'; last; }
		if($type == TAG_Int) { $spec = 'l>'; last; }
		if($type == TAG_Long)
			{
			# pack format "q" not supported on all platforms.
			my $la = int($topack[0] / 4294967296);
			my $lb = $topack[0] - ($la * 4294967296);
			my $tmp = pack($spec, @topack);
			print $fh $tmp or die('failed to write ' . CORE::length($tmp)
				. ' bytes for type ' . $type);
			return $tmp;
			}
		if($type == TAG_Float) { $spec = 'f>'; last; }
		if($type == TAG_Double) { $spec = 'd>'; last; }
		if($type == TAG_Byte_Array) { unshift @topack, CORE::length($val); $spec = 'l>a*'; last; }
		if($type == TAG_String) { unshift @topack, CORE::length($val); $spec = 's>a*'; last; }
		die('unrecognized value type ' . $type);
		}

	my $tmp = pack($spec, @topack);
	print $fh $tmp or die('failed to write ' . CORE::length($tmp) . ' bytes for type ' . $type);
	return $tmp;
	}

sub writetag
	{
	my $self = shift;
	my $fh = shift;
	my $nometa = shift;

	$nometa or writevalue($fh, TAG_Byte, $self->type);
	$nometa or writevalue($fh, TAG_String, $self->name);

		{
		if($self->type == 0) { }
		if(($self->type >= 1) && ($self->type <= 8))
			{
			writevalue($fh, $self->type, $self->value);
			last;
			}
		if($self->type == 9)
			{
			writevalue($fh, TAG_Byte, $self->subtype);
			if($self->subkeys())
				{
				writevalue($fh, TAG_Int, $self->length);
				foreach my $i ( @{$self->subkeys} )
					{
					$self->{$i}->writetag($fh, 1);
					}
				}
			else
				{
				writevalue($fh, TAG_Int, 0);
				}
			last;
			}
		if($self->type == 10)
			{
			if($self->subkeys())
				{
				foreach my $i ( @{$self->subkeys()} )
					{
					$self->{$i}->writetag($fh);
					}
				}
			writevalue($fh, TAG_Byte, 0);
			last;
			}
		die('unexpected tag type ' . $self->type);
		}
	}

sub newsubtag
	{
	my $self = shift();
	my $name = shift();
	my $type = shift();

	my $tag = new NuCraftd::NBTag();
	$tag->name($name);
	$tag->type($type);

		{
		if($type == 0) { last; }
		if(($type >= 1) && ($type <= 8))
			{
			$tag->value(shift());
			last;
			}
		if($type == 9)
			{
			$tag->subtype(shift());
			my $i = 0;
			while(scalar(@_))
				{
				$tag->newsubtag($i++, $tag->subtype(), shift());
				}
			last;
			}
		if($type == 10) { last; }
		die('unexpected tag type ' . $tag->type);
		}

	my @sk = ( );
	$self->subkeys() and push @sk, @{$self->subkeys()};
	$name ~~ @sk or push @sk, $name;
	$self->subkeys(\@sk);

	$self->{$name} = $tag;
	return $tag;
	}

sub remove
	{
	my $self = shift();
	my $name = shift();

	delete($self->{$name});

	my @newsub = ( );
	foreach my $sub ( @{$self->subkeys()} )
		{
		$sub eq $name or push @newsub, $sub;
		}
	$self->subkeys(\@newsub);
	}
