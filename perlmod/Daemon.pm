#!/usr/bin/perl

use strict;
use warnings;

package NuCraftd::Daemon;
our $VERSION = '1.0';

use Sys::Syslog qw( :standard :macros );
use IO::Handle;
use POSIX;
use Fcntl qw(:flock);
use NuCraftd::Config;
use NuCraftd::Util;

prodver('$Format:%h$');

use Exporter;
our @ISA = ('Exporter');
our @EXPORT = qw(
	status
	info
	infostat
	norestart
	daemonloop
	proclock
	);

my $daemonpid;
my $logopen;
my $autorestart;
my $finalproc;
my $processlock;

sub status
	{
	unshift @_, statusprefix();
	unshift @_, appname();
	$0 = join(' ', @_);
	}

sub mylog
	{
	my $sev = shift();
	$logopen
		and syslog($sev, logprefix() . join(' ', @_))
		or print STDERR join(' ', @_) . "\n";
	}
sub info
	{
	mylog(LOG_INFO, @_);
	}

sub infostat
	{
	status @_;
	info @_;
	}

sub norestart
	{
	$autorestart = undef;
	}

sub daemonloop
	{
	$daemonpid and die('daemonloop is not reentrant');

	my $bodysub = shift;
	my $shutdownproc = shift;
	my $restartproc = shift;
	my $stoppedproc = shift;
	$finalproc = shift;
	defined($shutdownproc) or
		$shutdownproc = sub
			{
			shift eq 'DIE' and exit 1;
			exit 0;
			};
	defined($restartproc) or
		$restartproc = sub
			{
			die('restart requested');
			};
	defined($stoppedproc) or $stoppedproc = sub { };
	defined($finalproc) or $finalproc = sub { };

	openlog(appname(),
		'ndelay,pid,nowait' . (debugmode() ? ',perror' : ''),
		LOG_DAEMON)
        	or die('failed to open syslog: ' . $!);
	$logopen = 1;

	if(!debugmode())
		{
		open NEWINP, '<', '/dev/null' or die $!;
		open NEWOUT, '>', '/dev/null' or die $!;
		open NEWERR, '>', '/dev/null' or die $!;
		STDIN->fdopen(\*NEWINP, 'r') or die $!;
		STDOUT->fdopen(\*NEWOUT, 'w') or die $!;
		STDERR->fdopen(\*NEWERR, 'w') or die $!;
		chdir('/');
		fork() and exit(0);
		setsid();
		}

	local %SIG;
	$SIG{'__DIE__'} = sub
		{
		mylog(LOG_ERR, 'exception:', @_);
		$shutdownproc->('DIE', @_);
		};
	$SIG{'__WARN__'} = sub
		{
		mylog(LOG_WARNING, 'warning:', @_);
		};
	$SIG{'HUP'} = $SIG{'PIPE'} = sub
		{
		mylog(LOG_INFO, 'signal:', @_);
		};
	$SIG{'INT'} = $SIG{'TERM'} = sub
		{
		mylog(LOG_INFO, 'signal:', @_);
		$shutdownproc->(@_);
		};
	$SIG{'CHLD'} = sub
		{
		my $kid;
		do
			{
			$kid = waitpid(-1, WNOHANG);
			}
			while $kid > 0;
		};

	$daemonpid = $$;
	status 'startup';
	info 'startup (' . prodline() . ')';

	$autorestart = 1;
	while($autorestart)
		{
		my $restime = time() + 10;
		eval
			{
			local $SIG{'__DIE__'};
			local $SIG{'HUP'} = sub
				{
				mylog(LOG_INFO, 'signal:', @_);
				$restartproc->(@_);
				};
			$bodysub->();
			};
		my $err = $@;
		if($daemonpid == $$)
			{
			$err and mylog(LOG_ERR, 'stopped:', $err)
				or mylog(LOG_ERR, 'stopped');
			}
		else
			{
			$err and mylog(LOG_ERR, 'stopped:', $err);
			last;
			}
		eval
			{
			$SIG{'__DIE__'} = sub
				{
				mylog(LOG_ERR, 'exception:', @_);
				};
			$stoppedproc->($@);
			};
		$autorestart or last;
		my $now = time();
		if($now < $restime)
			{
			my $sleep = int($restime - $now);
			infostat 'sleeping ' . $sleep . ' seconds';
			while($now < $restime)
				{
				$now = time();
				my $sleep = int($restime - $now);
				status('sleeping ' . $sleep . ' seconds');
				sleep(1);
				}
			}
		infostat 'restarting';
		}

	exit 0;
	}

END
	{
	if($daemonpid and $daemonpid eq $$)
		{
		infostat 'shutdown';
		$finalproc->();
		}
	}

sub proclock
	{
	open($processlock, '>', lockpath()) or die($!);
	flock($processlock, LOCK_EX | LOCK_NB) or die('failed to lock ' . lockpath()
		. '; is another instance running?  ' . $!);
	}

1;
