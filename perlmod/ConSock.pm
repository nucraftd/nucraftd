#!/usr/bin/perl

use strict;
use warnings;

package NuCraftd::ConSock;
our $VERSION = '1.0';

use Exporter;
use IO::Socket;
use IO::Select;
use Fcntl;
use Fcntl qw(:flock);
use Text::Wrap;
use NuCraftd::Base;
use NuCraftd::Config;
use NuCraftd::LockedFile;
use Digest::MD5 qw(md5_hex);
use NuCraftd::Util;

prodver('$Format:%h$');

our @ISA = ('NuCraftd::Base');

sub new
	{
	bless({ }, shift());
	}

sub sockhandle
	{
	shift()->getsetprop('sockhandle', @_);
	}

sub mypath
	{
	shift()->getsetprop('mypath', @_);
	}

sub online
	{
	shift()->getsetprop('online', @_);
	}

sub onlinelist
	{
	sort { uc($a) cmp uc($b) } (keys %{shift()->online()});
	}

sub onlinecount
	{
	scalar keys %{shift()->online()};
	}

sub version
	{
	shift()->getsetprop('version', @_);
	}

sub buffer
	{
	shift()->getsetprop('buffer', @_);
	}

sub nonblock
	{
	my $self = shift();
	my $nb = shift();

	my $sock = $self->sockhandle();
        my $flags = fcntl($sock, F_GETFL, 0);
        fcntl($sock, F_SETFL, $nb ? ($flags | O_NONBLOCK) : ($flags & ~O_NONBLOCK))
                or die ('set file handle nonblocking failed');
	}

sub open
	{
	my $self = shift();
	my $addr = shift();
	$addr or $addr = $self->mypath();
	$addr or $addr = sockpath();

        my $sock = new IO::Socket::UNIX(
                Peer => $addr,
                Type => SOCK_STREAM)
                or die('unable to open socket');

	$self->sockhandle() and close($self->sockhandle());
        $self->sockhandle($sock);

	$self->nonblock(1);
	$self->mypath($addr);
	$self->online({ });

	$self->read(60);
	$self->read(1);
	}

sub read
	{
	my $self = shift();
	my $timeout = shift();
	$timeout or $timeout = 0;

	my $sock = $self->sockhandle();
	$sock or die('no console connection');

	my $buff = $self->buffer();
	$buff or $buff = '';
	if(!($buff =~ m#(.*)\n#) and scalar(new IO::Select($sock)->can_read($timeout)))
		{
		my $data = '';
		my $rv = $sock->read($data, POSIX::BUFSIZ - length($buff), 0);
		defined($rv) and length($data) or die('lost console connection');
		$buff .= $data;
		}
	$self->buffer($buff);
		
	if($buff =~ m#(.*)\n#)
		{
		my $x = $1;
		$self->buffer(substr($buff, length($x) + 1));

		rxjoin($x, sub { $self->online()->{shift()} = 1; });
		rxpart($x, sub { delete($self->online()->{shift()}); });
		rxplist($x, sub
			{
			$self->online({ });
			while(my $p = shift())
				{
				$self->online()->{$p} = 1;
				}
			});
		rxversion($x, sub
			{
			my $v = shift();
			$v =~ s#\s+$##;
			$v =~ s#\s+# #g;
			$v =~ m#\S# and $self->version($v);
			});

		return $x;
		}

	return undef;
	}

sub readmany
	{
	my $self = shift();
	my $timeout = shift();
	my $readsub = shift();

	$readsub or die('read subroutine not defined');

	my $l;
	if($l = $self->read($timeout))
		{
		$readsub->($l);
		while($l = $self->read(0))
			{
			$readsub->($l);
			}
		}
	}

sub send
	{
	shift()->sockhandle()->send(join("\n", @_) . "\n");
	}

sub wrapsend
	{
	my $self = shift();
	my $cmd = shift();
	my $pref = shift();

	my $oldwrap = $Text::Wrap::columns;
	$Text::Wrap::columns = 100 + length($cmd);
	$pref = $cmd . ' ' . $pref . ' ';
	$pref =~ s#\s+# #g;
	foreach my $x (@_)
		{
		my $x = wrap($pref, $pref, $x);
		$x =~ s#\t#  #g;
		$self->send($x);
		}
	$Text::Wrap::columns = $oldwrap;
	}

sub queuelf
	{
	my $self = shift();
	my $user = shift();
	my $d = temppath() . '/msgqueue';
	checkdir($d);
	new NuCraftd::LockedFile($d . '/' . md5_hex(lc($user)) . '.txt', LOCK_EX);
	}

sub queuemsg
	{
	my $self = shift();
	my $user = shift();

	my $lf = $self->queuelf($user);
	my @msgs = [ ];
	$lf->createfile(sub { my $fh = shift(); @msgs = <$fh>; });
	push @msgs, split("\n", join("\n", @_));
	while(scalar(@msgs) > 25)
		{
		shift @msgs;
		}
	$lf->writefile(sub { my $fh = shift(); print $fh join("\n", @msgs) . "\n"; });
	}

sub tell
	{
	my $self = shift();
	my $user = shift();

	$self->online()->{$user}
		and map { $self->wrapsend('tell ' . $user, '', $_); } @_
		or $self->queuemsg($user, @_);
	}

sub wraptell
	{
	my $self = shift();
	my $user = shift();
	my $pref = shift();

	my $oldwrap = $Text::Wrap::columns;
	$Text::Wrap::columns = 100;
	$pref = $pref . ' ';
	$pref =~ s#\s+# #g;
	foreach my $x (@_)
		{
		$self->tell($user, split(m#\n+#, wrap($pref, $pref, $x)));
		}
	$Text::Wrap::columns = $oldwrap;
	}

sub queuesend
	{
	my $self = shift();

	foreach my $user ( keys %{$self->online()} )
		{
		my $lf = $self->queuelf($user);
		-s $lf->path() or next;
		my @msgs = [ ];
		$lf->readfile(sub { my $fh = shift(); @msgs = <$fh>; });
		map { m#\S# and $self->tell($user, '', $_); } @msgs;
		$lf->writefile(sub { });
		}
	}

sub DESTROY
	{
	my $sock = shift()->sockhandle();
	$sock and close($sock);
	}

1;
