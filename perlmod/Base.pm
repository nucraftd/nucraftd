#!/usr/bin/perl

use strict;
use warnings;

package NuCraftd::Base;
our $VERSION = '1.0';

use Exporter;
use NuCraftd::Util;

prodver('$Format:%h$');

our @ISA = ('Exporter');

sub getsetprop
	{
	my $self = shift;
	my $prop = shift;
	my $newval = shift;
	my $oldval = $self->{$prop};
	defined($newval) and $self->{$prop} = $newval;
	return $oldval;
	}
