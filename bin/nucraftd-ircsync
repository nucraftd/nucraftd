#!/usr/bin/perl -w

=head1 NAME

nucraftd-ircsync - Relay chat between game world and IRC channel

=head1 COPYRIGHT

Copyright (C)2013 Aaron Suen <warr1024@gmail.com>

Permission to use, copy, modify, and/or distribute this software for
any purpose with or without fee is hereby granted, provided that the
above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR
BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES
OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

=head1 DESCRIPTION

This component relays chat messages and other events between the game
console and one or more Internet Relay Chat channels.

The following messages are relayed from the game world to the IRC
channel:

=over 8

=item Player connects, disconnects, and presence.

=item Player public chat.

=item Player emotes (Not working due to upstream bug:
B<https://mojang.atlassian.net/browse/MC-4640>).

=item Player deaths.

=back

The following messages are relayed from the IRC chanel into the
game world:

=over 8

=item User joins, parts, quits, and presence.

=item User channel messages.

=item User emotes.

=back

=cut

use strict;
use warnings;
use diagnostics qw(-t -w);

# Disable POE built-in exception handling, forcing the bot
# to reset itself via nucraftd exception handling.
sub POE::Kernel::CATCH_EXCEPTIONS () { 0 }

use POSIX;
use IO::Select;
use NuCraftd::Config;
use NuCraftd::Daemon;
use NuCraftd::ConSock;
use NuCraftd::Util;
use Bot::BasicBot;

prodver('$Format:%h$');

=head1 SYNOPSIS

nucraftd-ircsync [B<-c> I<ircchannel> [B<-c> I<ircchannel>]
...]]  [B<-D>] [B<-n> I<ircnick>] [B<-N> I<ircrealname>] [B<-p>
I<ircport>] [B<-P> I<nickservpass>] [B<-r> I<rootpath>] [B<-R>]
[B<-s> I<ircserver>] [B<-t> I<temppath>] [B<-u> I<ircusername>]
[B<-w> I<world>]

nucraftd-ircsync B<-V>

=head1 OPTIONS

=over 8

=item B<-c> I<ircchannel>

Add the IRC channel I<ircchannel>.  The channel name should be
specified including the leading #.  If multiple channels are specified,
then the IRC relay bot will sit in all channels and relay messages
to/from each.  If no channels are specified, it will default to the
C<#nucraftd> channel.

=item B<-D>

Enable debugging.  Do not daemonize, and write logs to STDERR.

=item B<-n> I<ircnick>

Set the nickname of the IRC bot to I<ircnick>.  This is the name by
which the bot will be known and will display in channels.  If not
specified, the default is C<nucraftd-I<world>>, where I<world> is the
name of the world.

=item B<-N> I<ircrealname>

Set the IRC "real name" field for the bot.  This defaults to
C<nucraftd>.

=item B<-p> I<ircport>

Set the IRC port to which to connect to I<ircport>.  The default
is C<6667>.

=item B<-P> I<nickservpass>

Specify a NickServ password for authenticating on an IRC network that
uses Services.  If a non-blank I<nickservpass> option is specified,
the bot will issue a private message C<identify I<nickservpass>> to
NickServ after connecting, to authenticate as the legitimate owner
of its nickname.  This can be used to provide nickname protection
for the bot.

=item B<-r> I<rootpath>

Set the root path, at which nucraftd data will be written to permanent
storage (e.g. HDD).  The default is C<~/.nucraftd>.  Note that if
different components use different values for this, they will be unable
to communicate properly with one another, allowing you to have multiple
"groups" of worlds; be sure to set I<temppath> to something different
as well.

=item B<-R>

Allow running as root.  Normally, this is disabled.  I<This is an
important safety feature and should not be disabled under almost
any circumstance.>

=item B<-s> I<ircserver>

IRC server to which to connect.  Defaults to C<localhost>.

=item B<-t> I<temppath>

Set the temporary path for nucraftd locks, sockets, and internal
communication.  The default is C</tmp/nucraftd-I<username>>.  Note that
if different components use different values for this, they will be
unable to communicate properly with one another, allowing you to have
multiple "groups" of worlds; be sure to set I<rootpath> to something
different as well.

=item B<-u> I<ircusername>

Set the IRC "user name" field for the bot.  This defaults to
C<nucraftd>.

=item B<-V>

Display nucraftd version and exit.

=item B<-w> I<world>

Set the world name.  The default is C<default>.  Multiple instances
can run with different world names, to enable multi-world.

=back

=cut

my $ircserver = 'localhost';
my $ircport = 6667;
my @channels = ( );
my $nick = '';
my $username = '';
my $realname = '';
my $nickpass = '';
configload(
	"s=s" => \$ircserver,
	"p=i" => \$ircport,
	"c=s@" => \@channels,
	"n=s" => \$nick,
	"u=s" => \$username,
	"N=s" => \$realname,
	"P=s" => \$nickpass);
@channels or push @channels, '#' . prodname();
$nick or $nick = prodname() . '-' . ucfirst(world());
$username or $username = prodname();
$realname or $realname = prodname();

proclock();

# Create McBot as a subclass of BasicBot, as opposed to the normal
# process of making the entire script a subclass of BasicBot as defined
# in the BasicBot docs.
eval
	{
	package McBot;
	use base 'Bot::BasicBot';

	use NuCraftd::ConSock;
	use NuCraftd::Util;

	# Event from the bot after connection.
	sub connected
		{
		my $self = $_[0];
		
		# Set the last event timestamp, for self-pinging.
		$self->{lastevt} = time();

		# Identify with NickServ.
		$nickpass and $self->say(who => 'nickserv', channel => 'msg',
			body => 'IDENTIFY ' . $nickpass);

		# Start the 1-second ticking timer.
		$self->schedule_tick(1);

		$self->{infostat}('running');
		}

	# Broadcast a message from the bot to all IRC channels.
	sub bcast
		{
		my ($self, $body) = @_;
		for my $c ( @channels )
			{
			$self->say(channel => $c, body => $body);
			}
		}

	# Request updated name lists for all channels. 
	sub getallnames
		{
		my $self = $_[0];
		for my $c ( @channels )
			{
			$self->names($c);
			}
		}

	# Announce the online players to all IRC channels.
	sub announceonline
		{
		my $self = $_[0];
		my $ol = join(', ', $self->{consock}->onlinelist());
		$ol and $self->bcast('Online: ' . $ol);
		}

	# Announce the IRC users to the game players, and possibly announce
	# game players to newly-joining IRC users.
	sub announcenames
		{
		my $self = $_[0];
		
		# Figure out who is in IRC, including whether the bot itself
		# is there.
		my %who = ( );
		my $onchan = '';
		for my $c ( @channels )
			{
			my $raw = $self->channel_data($c);
			$raw or next;
			my %chan = %{$raw};
			for my $n ( keys %chan )
				{
				$n eq $self->nick and $onchan = 1 and next;
				$n =~ s#_+$##;
				$n =~ s#^_+##;
				$who{$n} = 1;
				}
			}

		# If we had been in the channel previously, but were kicked
		# out, die and restart.  If we're connecting to the channel
		# for the first time, let the players know that IRC is connected.
		$self->{onchan} and ($onchan or die('kicked from channel'));
		if($onchan and !$self->{onchan})
			{
			$self->{consock}->send('say IRC relay connected.');
			$self->{onchan} = 1;
			}

		# Compute list of all IRC users, and determine if that list is
		# different from the last-announced one.
		my $list = join(', ', sort { lc($a) cmp lc($b) } (keys %who));
		if($list ne $self->{list})
			{
			my %old = map { $_ => 1 } split(m#,\s*#, $self->{list});
			
			# Check for any newly-joined IRC users.  Announce to players
			# any new IRC users, and announce to new IRC users presence
			# of players.
			my %join = ( );
			for my $n ( sort keys %who )
				{
				$old{$n} or $join{$n} = 1;
				}
			if(scalar(keys %join))
				{
				$self->announceonline();
				$self->{consock}->wrapsend('say', 'Joined:', join(', ',
					sort { lc($a) cmp lc($b) } (keys %join)));
				}

			# Check for IRC users that have parted and announce them to
			# game players.
			my %part = ( );
			for my $n ( sort keys %old )
				{
				$who{$n} or $part{$n} = 1;
				}
			scalar(keys %part) and
				$self->{consock}->wrapsend('say', 'Parted:',
				join(', ', sort { lc($a) cmp lc($b) } (keys %part)));

			# Schedule an announcement of the new IRC user list to players.
			# This is not done immediately to coalesce multiple joins/parts
			# into a single announcement.
			$self->{listtime} = time() + 2;
			}
		$self->{list} = $list;
		}

	# Tick timer, executed every second, which handles delayed and
	# periodic events, and processes messages from the game server.
	sub tick
		{
		my $self = $_[0];
		my $now = time();
		
		# Check the last event timestamp.  If we haven't had any
		# communication with the IRC server for a full minute, send a
		# "ping" message to keep the connection alive.  If we haven't
		# heard anything for a minute and a half, assume we must have
		# lost the connection somehow and restart.
		my $le = $self->{lastevt};
		$le or ($le, $self->{lastevt}) = ($now, $now);
		$le < ($now - 90) and die('event timeout');
		$le < ($now - 60) and $self->say(who => $self->nick,
			channel => 'msg', body => 'ping');

		# Handle delayed IRC user announcements.
		if($self->{listtime} and ($self->{listtime} < $now))
			{
			$self->{consock}->wrapsend('say', 'On IRC:', $self->{list});
			delete($self->{listtime});
			}
			
		# Touch our lockfile, to make sure that cronjobs don't
		# start cleaning it up automatically.
		utime undef, undef, $self->{lockpath}();

		my $ol = join('|', map { quotemeta($_) } $self->{consock}->onlinelist());
		# Process logs from world to irc.
		$self->{consock}->readmany(0, sub
			{
			my $l = constrip(shift());

			# Process chats, emotes, joins and parts from the game world.
			rxchat($l, sub
				{
				my ($who, $what) = @_;
				$self->bcast('<' . $who . '> ' . $what);
				}) or
			rxemote($l, sub
				{
				my ($who, $what) = @_;
				$self->bcast('* ' . $who . ' ' . $what);
				}) or
			rxjoin($l, sub
				{
				my $who = $_[0];
				$self->bcast('Joined: ' . $who);
				$self->announceonline();
				$self->{list} and $self->{consock}->wrapsend('say',
					'On IRC:', $self->{list});
				}) or
			rxpart($l, sub
				{
				my $who = $_[0];
				$self->bcast('Parted: ' . $who);
				$self->announceonline();
				return 1;
				}) or
			# Death messages are pretty freeform, but generally start
			# with a player name, but are not part messages (those are
			# trapped first above).
			$l =~ m#^\s*\[INFO\]\s+(($ol)\s+\S.*)$# and $self->bcast($1);
			});

		$self->announcenames();
		$self->{consock}->queuesend();

		return 1;
		}

	# Event when an IRC user changes nickname.
	sub nick_change
		{
		my ($self, $old, $new) = @_;
		$self->{lastevt} = time();
		$old =~ s#_+$##;
		$old =~ s#^_+##;
		$new =~ s#_+$##;
		$new =~ s#^_+##;
		$self->{consock}->wrapsend('say', $old
			. ' is now know as ' . $new);
		return undef;
		}

	# Event when an IRC user speaks.
	sub said
		{
		my ($self, $msg) = @_;
		$self->{lastevt} = time();
		my $body = $msg->{body};
		my $n = $msg->{who};
		$n eq $self->nick and return;
		$n =~ s#_+$##;
		$n =~ s#^_+##;
		$msg->{address} and ($msg->{address} eq 'msg'
			or $body = $msg->{address} . $body);
		$self->{consock}->wrapsend('say', '<' . $n . '>', $body);
		$body =~ m#^\s*Joined:\s+\S# and $self->announceonline();
		return undef;
		}

	#Event when an IRC user emotes.
	sub emoted
		{
		my ($self, $msg) = @_;
		$self->{lastevt} = time();
		my $body = $msg->{body};
		my $n = $msg->{who};
		$n eq $self->nick and return;
		$n =~ s#_+$##;
		$n =~ s#^_+##;
		$msg->{address} and ($msg->{address} eq 'msg'
			or $body = $msg->{address} . $body);
		$self->{consock}->wrapsend('say', '* ' . $n . ' ', $body);
		return undef;
		}

	# Event when IRC user joins channel; request names and handle
	# join/part announcement based on difference in list instead of
	# trusting this event implicitly.
	sub chanjoin
		{
		my ($self, $msg) = @_;
		$self->{lastevt} = time();
		$self->getallnames();
		return undef;
		}

	# Event when IRC user leaves channel; request names and handle
	# join/part announcement based on difference in list instead of
	# trusting this event implicitly.
	sub chanpart
		{
		my ($self, $msg) = @_;
		$self->{lastevt} = time();
		$self->getallnames();
		return undef;
		}

	# Event when someone is kicked from channel; request names and handle
	# join/part announcement based on difference in list instead of
	# trusting this event implicitly.
	sub kicked
		{
		my ($self, $msg) = @_;
		$self->{lastevt} = time();
		$self->getallnames();
		return undef;
		}

	# Event when IRC user leaves server; request names and handle
	# join/part announcement based on difference in list instead of
	# trusting this event implicitly.
	sub userquit
		{
		my ($self, $msg) = @_;
		$self->{lastevt} = time();
		$self->getallnames();
		return undef;
		}

	# Event for when IRC bot is finished obtaining an update list of
	# names in a channel.
	sub got_names
		{
		my $self = $_[0];
		$self->{lastevt} = time();
		$self->announcenames();
		}

	# Event for when the IRC connection is lost.
	sub irc_disconnected_state
		{
		my $self = $_[0];
		die('lost irc connection');
		}

	# Event for when there is an IRC protocol error.
	sub irc_error_state
		{
    		my ( $self, $err, $kernel ) = @_[ 0, 10, 2 ];
		die('irc error: ' . $err);
		}
	};

my $pid = 0;
daemonloop(sub
	{
	status('master');
	
	# Fork the actual bot as a separate background process.  This
	# allows the bot to crash out completely, while the original master
	# process continues to act as a watchdog, since the POE abstraction
	# layer does some odd stuff with signal handling that may not work
	# completely with the deaemonloop signal handlers.
	$pid = fork();
	if(!$pid)
		{
		# Reset child signal handlers.
		for my $s ( qw (TERM INT HUP ALRM PIPE __DIE__ CHLD) )
			{
			$SIG{$s} = 'DEFAULT';
			}

		# Connect to game console.
		infostat('connecting to console');
		my $sock = new NuCraftd::ConSock();
		$sock->open();

		# Configure the IRC bot.
		my $bot = McBot->new(
			server => $ircserver,
			port => $ircport,
			channels => \@channels,
			nick => $nick,
			username => $username,
			realname => $realname
		);
		$bot->{infolog} = sub { info(@_) };
		$bot->{infostat} = sub { infostat(@_) };
		$bot->{lockpath} = sub { lockpath(@_) };
		$bot->{consock} = $sock;
		my %empty = ( );
		$bot->{namecache} = \%empty;
		$bot->{list} = '';

		# Connect IRC bot and let its event internal loop take over.
		infostat('connecting to irc');
		$bot->run();
		exit(0);
		}

	# Wait for the IRC bot to exit.
	waitpid($pid, 0);
	},
sub
	{
	$pid and kill('TERM', $pid);
	shift eq 'DIE' and exit 1;
	exit 0;
	},
undef,
sub
	{
	$pid and kill('TERM', $pid);
	my $sock = new NuCraftd::ConSock();
	$sock->open();
	$sock->send('say IRC relay disconnected.');
	});

=head1 SEE ALSO

L<nucraftd-backup(1)>,
L<nucraftd-server(1)>,
L<nucraftd-update(1)>,
L<nucraftd-watchdog(1)>
