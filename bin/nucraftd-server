#!/usr/bin/perl -w

=head1 NAME

nucraftd-server - Wrapper for Minecraft SMP server

=head1 COPYRIGHT

Copyright (C)2013 Aaron Suen <warr1024@gmail.com>

Permission to use, copy, modify, and/or distribute this software for
any purpose with or without fee is hereby granted, provided that the
above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR
BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES
OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

=head1 DESCRIPTION

This component is the central nucraftd server, which wraps and runs
the Minecraft SMP server.  The Minecraft server will be restarted
automatically if it crashes or is stopped.  Access to the console
is provided to other nucraftd components (or users with appropriate
permissions, via netcat) on a Unix-domain socket.

=cut


use strict;
use warnings;
use diagnostics qw(-t -w);

use Tie::RefHash;
use IPC::Open3;
use IO::Socket;
use IO::Select;
use Fcntl;
use POSIX;
use Digest::MD5;
use NuCraftd::Config;
use NuCraftd::Daemon;
use NuCraftd::Util;

prodver('$Format:%h$');

our ($ram, $jre, $jar, @oparams);

=head1 SYNOPSIS

nucraftd-server [B<-D>] [B<-j> I<server.jar>] [B<-m> I<ram>] [B<-r>
I<rootpath>] [B<-R>] [B<-t> I<temppath>] [B<-w> I<world>] [B<-x>
I<jrepath>]

nucraftd-server B<-V>

=head1 OPTIONS

=over 8

=item B<-D>

Enable debugging.  Do not daemonize, and write logs to STDERR.

=item B<-j> I<server.jar>

Specify path to the C<minecraft_server.jar> file.  This can be
downloaded from L<http://www.minecraft.net/download.jsp>.  Note that
you may also be able to run custom or modified servers, as long as
their console interface is compatible with the original Minecraft
SMP server; some features may not work, or may work differently.
The default path is C<I<rootpath>/I<world>/server.jar>.

=item B<-m> I<ram>

Specify the number of megabytes to pass to the Java runtime, via -Xms
and -Xmx.  This sets the target amount of RAM used by the Java internal
heap, excluding overhead from Java itself.  Higher values require
more memory, but may improve server performance.  Default is 1024.

=item B<-p> I<param>

Pass an arbitrary parameter to the java runtime.  Multiple use of this
option allow multiple parameters to be passed in the specified order.
Parameters are not split on whitespace.  For instance, to pass the
parameters C<-Xtune virtualized> to the runtime, specify C<-p -Xtune
-p virtualized>.  Note that the C<-Xms> and C<-Xmx> options are already
handled by the C<-m> option to nucraftd, and the C<-jar> option is
handled by the C<-j> option to nucraftd.

=item B<-r> I<rootpath>

Set the root path, at which nucraftd data will be written to permanent
storage (e.g. HDD).  The default is C<~/.nucraftd>.  Note that if
different components use different values for this, they will be unable
to communicate properly with one another, allowing you to have multiple
"groups" of worlds; be sure to set I<temppath> to something different
as well.

=item B<-R>

Allow running as root.  Normally, this is disabled.  I<This is an
important safety feature and should not be disabled under almost
any circumstance.>

=item B<-t> I<temppath>

Set the temporary path for nucraftd locks, sockets, and internal
communication.  The default is C</tmp/nucraftd-I<username>>.  Note that
if different components use different values for this, they will be
unable to communicate properly with one another, allowing you to have
multiple "groups" of worlds; be sure to set I<rootpath> to something
different as well.

=item B<-V>

Display nucraftd version and exit.

=item B<-w> I<world>

Set the world name.  The default is C<default>.  Multiple instances
can run with different world names, to enable multi-world.

=item B<-x> I<jrepath>

Specify path to the Java runtime executible.  The default is to
search your $PATH for a "java" executible.  On some platforms, such
as OpenBSD, the java runtime executible is not installed by default
in a location that is normally in $PATH.

=back

=cut

# Some configuration parameters.  These would probably benefit from being
# read from a configuration file.
configload(
	'm=s' => \$ram,
	'x=s' => \$jre,
	'j=s' => \$jar,
	'p=s@' => \@oparams);
$ram or $ram = 1024;

# Set process title.
status('initializing');

# Make sure no other instances can run for the same world.
proclock();

# Set CWD to where we want the server to store its data.
chdir(datapath()) or die($!);

# Try to find the server jar file.
$jar or $jar = datapath() . '/server.jar';
-f $jar or die('could not find minecraft server.jar');

# JRE needs to be installed.
$jre or $jre = findexec('java');
-x $jre or die('"' . $jre . '" does not point to an executible');

# Open a socket to listen for plugin connections.  Plugins can monitor
# all server log messages (they receive copies of every console line),
# and can also issue server messages.  Do this before forking, so that
# we can connect plugins immediately after server startup returns.
my $omask = umask(077);
our $listen = new IO::Socket::UNIX(Listen => 5, Local => sockpath()) or die($!);
chown $>, $), sockpath();
chmod 0660, sockpath();
umask($omask);

# Subroutine to set a file handle as non-blocking.
sub nonblock
	{
	while(my $fh = shift)
		{
		my $flags = fcntl($fh, F_GETFL, 0);
		fcntl($fh, F_SETFL, $flags | O_NONBLOCK)
			or die ('set file handle nonblocking failed');
		}
	}

# Keep track of the actual java server process ID, so we can signal it in
# sync with this wrapper.
our $pid = 0;

# Make the server's console handles available to signal handlers.
our $svr_in;
our $svr_out;
our $svr_err;

# Different methods of stopping the server.  When a stop is requested,
# each of them is tried in turn, with the last one being repeated.
our @stoptypes = ();
sub dostop()
	{
	my $stop = shift(@stoptypes);
	scalar(@stoptypes) or push @stoptypes, $stop;
	$stop->();
	}

daemonloop(sub
	{
	status('starting');

	# Write out PID for the server wrapper to a file.
	my $pidpath = temppath() . '/' . world() . '.pid';
	my $pidfh;
	open($pidfh, '>', $pidpath) and print $pidfh $$;
	close($pidfh);

	# Daemonizing resets CWD; reset it each cycle.
	chdir(datapath()) or die($!);

	# Before starting the server, symlink the server.log file into oblivion,
	# since we're going to catch console messages and write them to syslog anyway.
	-e 'server.log' or symlink('/dev/null', 'server.log');

	# Get the "exact" jar version, i.e. a portion of the MD5 of the full jar
	# file.  This identifies different builds of the server that have the same
	# advertised version, for more exact bug tracing, and to assist updates.
	my $jarhash = 'unk';
	eval
		{
		my $jarfh;
		open($jarfh, '<', $jar);
		my $jarmd = new Digest::MD5();
		$jarmd->addfile($jarfh);
		close($jarfh);
		$jarhash = '0x' . substr($jarmd->hexdigest(), 0, 7);
		};

	# Fork off the minecraft server, attached to pipes to trap its I/O.
	$pid = open3($svr_in, $svr_out, $svr_err, 
		$jre, '-Xmx' . $ram. 'M', '-Xms' . $ram . 'M', '-jar', $jar, @oparams, 'nogui');
	$svr_err or $svr_err = $svr_out;
	nonblock($svr_in, $svr_out, $svr_err);
	info('java pid', $pid);

	# Setup stop signal types.
	@stoptypes = (
		#sub { syswrite($svr_in, "stop\n"); },
		sub { kill('TERM', $pid); },
		sub { kill('KILL', $pid); }
		);

	# Setup a select to asynchronously handle input from the Unix socket
	# and from the java process.  Don't add the unix-domain listener until
	# the minecraft server has given us a version and online status, as
	# this seems to cause a race with modules connecting before this data
	# is available.
	our $select = new IO::Select($svr_in, $svr_out, $svr_err);
	my $listenready = '';

	# Create "tied" hashes (can use file descriptor hashes as keys)
	# to represent raw input and output buffers, a buffer of completed
	# lines (everything is raw-buffered until a line is completed),
	# and valid clients (i.e. to whom we broadcast server messages).
	sub tiedhash
		{
		my %n = ();
		tie %n, 'Tie::RefHash';
		return %n;
		}
	our %inbuff = tiedhash();
	our %outbuff = tiedhash();
	our %ready = tiedhash();
	our %clients = tiedhash();

	# Subroutine for closing a connection or file handle, including
	# unbinding it from the various places into which it's hooked.
	sub closeconn
		{
		my $fh = shift;
		defined($fh) or return;
		if($fh eq $svr_out or $fh eq $svr_err or $fh eq $svr_in)
			{
			die('server console disconnected');
			}
		delete $inbuff{$fh};
		delete $outbuff{$fh};
		delete $ready{$fh};
		delete $clients{$fh};
		$select->remove($fh);
		close($fh);
		}

	# Keep some server status and version data, and replay it to any
	# newly-connecting clients, so they know this data as soon as they
	# connect to the socket.
	my $online = '';
	my $version = '';
	my $olcount = 0;

	# If we haven't seen a player list in a certain amount of time, request
	# one, to be sure we're up-to-date.
	my $listexpire = 0;

	# Shutdown-on-empty status.
	my $hadplayers = '';
	my $emptytime = 0;

	status('running');

	# Run until the minecraft server dies.
	while(kill(0, $pid))
		{
		# Update the modtime on files in /tmp/.  This is because OpenBSD's
		# daily cronjob will delete "stale" files based on mtime/atime,
		# which could inadvertently clear our lock.
		utime undef, undef, lockpath(), sockpath(), $pidpath;

		# If we have the server version and list of online players, we can
		# fulfill the requirement of sending these as the first two lines
		# of plugin socket connections, so enable them.
		if($online and $version and !$listenready)
			{
			$select->add($listen);
			$listenready = 1;
			}

		# Process all available incoming data on all connections.
		foreach my $fh ($select->can_read(1))
			{
			if($fh == $listen)
				{
				# New connections to the listening socket need to be
				# accepted, which creates new client handles.
				my $new = $listen->accept;
				nonblock($new);
				$select->add($new);
				$clients{$new} = 1;
				$outbuff{$new} = $version . "\n" . $online . "\n";
				}
			else
				{
				# Avoid warning trying to read a write-only handle.
				$fh eq $svr_in and next;

				# Incoming data from client sockets or from the server's
				# I/O handles gets accumulated into the raw buffers.
				my $data = '';
				my $rv = fileno($fh) ? read($fh, $data, POSIX::BUFSIZ)
					: $fh->read($data, POSIX::BUFSIZ, 0);
				if(!defined($rv) || !length($data))
					{
					# Abnormal result from reading from stream;
					# close the connection or stream.
					closeconn($fh);
					next;
					}
				$inbuff{$fh} .= $data;

				# Copy whole lines into a second buffer, leaving behind
				# any incomplete lines.
				while($inbuff{$fh} =~ s#(.*\n)##)
					{
					$ready{$fh} .= $1;
					if($clients{$fh})
						{
						my $l = $1;
						chomp($l);
						info('input: ' . $l);
						}
					}
				}
			}

		# Process all completed lines from each connection.
		foreach my $fh ( keys %ready )
			{
			# Pull data from the buffer.
			my $data = $ready{$fh};
			delete $ready{$fh};

			if($fh eq $svr_out or $fh eq $svr_err)
				{
				# Detect the server version line, and append the hash.
				$data =~ s#(\[INFO\]\s+Starting\s+minecraft\s+server\s+version.*?)$#$1 \Q$jarhash\E#im;

				# For data coming from the server console, broadcast to all
				# connected clients.
				foreach my $s ( keys %clients )
					{
					$outbuff{$s} = ($outbuff{$s} or '') . $data;
					}

				# Split the data into individual lines.  If any of them is in
				# a "log" format, then trim off the date/time (which syslog will
				# re-add anyway) and forward to syslog.
				chomp($data);
				foreach my $l ( split(m#\n#, $data) )
					{
					$l = constrip($l);

					# If we're expecting an online player listing, assume it immediately
					# follows is heading, and break the resulting names out and store
					# them in the old, more easily-scriptable format.  Send them out to
					# connected clients in the old format immediately.
					if($olcount and $l =~ m#\[INFO\]\s*(.*)#i)
						{
						my @olp = map { s#^\s+##; s#\s+$##; $_; } split(/,/, $1);
						$olcount -= scalar(@olp);
						$online =~ m#:$# or $online .= ',';
						$online .= ' ' . join(', ', @olp);
						if($olcount < 1)
							{
							$listexpire = time() + 900;
							foreach my $s ( keys %clients )
								{
								$outbuff{$s} = ($outbuff{$s} or '') . $online . "\n";
								}
							}
						}

					# Detect a player login or disconnect notice.  On such notice,
					# issue a list, so we can see WHICH players connected or
					# disconnected, and update the online list.  I tried tracking this
					# just from login/logout messages, but it was too unreliable.
					rxjoin($l, sub { $listexpire = 0; });
					rxpart($l, sub { $listexpire = 0; });

					# Detect listings of currently-online users from minecraft
					# v1.2 and earlier, triggered by a list command from the console,
					# which, in turn, is triggered by a user logging in or out.
					rxplist($l, sub { $online = $l; $listexpire = time() + 900; });

					# Detect the beginning of a v1.3+ online player listing.  The new
					# server console complicates this by breaking it into multiple lines.
					if($l =~ m#\[INFO\]\s*There\s+are\s+(\d+)/\d+\s+players\s+online:\s*$#i)
						{
						$online = '[INFO] Connected players:';
						$olcount = $1;
						$olcount or $listexpire = time() + 900;
						}

					# Detect the server version line, and track it for all new
					# plugin connections.
					rxversion($l, sub { $version = $l; });

					# Log all messages.
					info('console: ' . $l);
					}
				}

			if($clients{$fh})
				{
				# For data coming from clients, forward them to the server
				# console to be executed.
				$outbuff{$svr_in} = ($outbuff{$svr_in} or '') . $data;
				}
			}

		# Request a list of online players if the current list is expired.
		# After a request, bump the expiration to give the server a grace period
		# to respond, before a re-request.
		my $listtime = time();
		if($listexpire < $listtime)
			{
			$outbuff{$svr_in} = ($outbuff{$svr_in} or '') . "list\n";
			$listexpire = $listtime + 15;
			}

		# Process all output buffers.
		foreach my $fh ($select->can_write(0))
			{
			# If a descriptor is ready to accept input, but we don't have
			# any input for it, then skip it.
			exists $outbuff{$fh} or next;

			# Try to write data to the stream.
			my $data = $outbuff{$fh};
			my $rv = fileno($fh) ? syswrite($fh, $data) : $fh->send($data, 0);
			defined($rv) or next;
			if(($rv == length($data))
				or ($! == POSIX::EWOULDBLOCK))
				{
				# Data was written normally or partially; trim from
				# the output buffer.
				substr($outbuff{$fh}, 0, $rv) = '';
				length($outbuff{$fh}) or delete $outbuff{$fh};
				}
			else
				{
				# Abnormal failure: close the connection and
				# forget about it from now on.
				closeconn($fh);
				}
			}
		}
	},
	sub
	{
		status('stopping');
		norestart();
		dostop();
	},
	sub
	{
		status('restart signal');
		dostop();
	},
	sub
	{
		local $SIG{'CHLD'} = $SIG{'PIPE'};
		if($pid and kill('KILL', $pid))
			{
			infostat('killing pid ' . $pid);
			waitpid($pid, 0);
			}
		$pid = 0;
	},
	sub
	{
		unlink(sockpath());
	});

=head1 SEE ALSO

L<nucraftd-backup(1)>,
L<nucraftd-ircsync(1)>,
L<nucraftd-update(1)>,
L<nucraftd-watchdog(1)>
