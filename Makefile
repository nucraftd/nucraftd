INSTALL	:= install -o 0 -g 0
MKDIR	:= mkdir -p -m 755
PREFIX	:= /usr/local
BINDIR	:= $(PREFIX)/bin
SHRDIR	:= $(PREFIX)/share

install: installmain installutil

installmain: installmod installbin

installmod:
	make -e 'MODDIR='`perl -e 'for $$_ (@INC) { m#^\Q$(PREFIX)\E.*/site_perl$$# and print and exit }'` installmodenv

installmodenv:
	@[ -n "$(MODDIR)" ] || exit 1
	$(MKDIR) $(MODDIR)
	$(MKDIR) $(MODDIR)/NuCraftd
	$(INSTALL) -m 644 perlmod/Base.pm $(MODDIR)/NuCraftd
	$(INSTALL) -m 644 perlmod/Config.pm $(MODDIR)/NuCraftd
	$(INSTALL) -m 644 perlmod/Daemon.pm $(MODDIR)/NuCraftd
	$(INSTALL) -m 644 perlmod/ConSock.pm $(MODDIR)/NuCraftd
	$(INSTALL) -m 644 perlmod/NBTag.pm $(MODDIR)/NuCraftd
	$(INSTALL) -m 644 perlmod/LockedFile.pm $(MODDIR)/NuCraftd
	$(INSTALL) -m 644 perlmod/NBTFile.pm $(MODDIR)/NuCraftd
	$(INSTALL) -m 644 perlmod/Util.pm $(MODDIR)/NuCraftd

installutil:
	$(MKDIR) $(BINDIR)
	$(INSTALL) -m 755 util/nbtdump $(BINDIR)
	$(INSTALL) -m 755 util/pnmtoschematic $(BINDIR)
	$(INSTALL) -m 755 util/mcplayerwatch $(BINDIR)

installbin:
	$(MKDIR) $(BINDIR)
	$(INSTALL) -m 755 bin/nucraftd-backup $(BINDIR)
	$(INSTALL) -m 755 bin/nucraftd-ircsync $(BINDIR)
	$(INSTALL) -m 755 bin/nucraftd-update $(BINDIR)
	$(INSTALL) -m 755 bin/nucraftd-server $(BINDIR)
	$(INSTALL) -m 755 bin/nucraftd-watchdog $(BINDIR)
